#!/usr/bin/env python

from operator import itemgetter

DIMENSIONS = 2

class Tree:
	def __init__(self, left, right, median, axis):
		self.left = left
		self.right = right
		self.median = median
		self.axis = axis
	
	def getPointsInRange(self, bottom_left, top_right):
		result = []
		if bottom_left[0] <= self.median[0] and self.median[0] <= top_right[0] \
				and bottom_left[1] <= self.median[1] and self.median[1] <= top_right[1]:
			print 'ADD_DP %f %f' % self.median
			print 'PLOT'
			result += [self.median]
		
		
		start = list(self.median)
		if hasattr(self.left, 'median'):
			start[self.left.axis] = self.left.median[self.left.axis]
		direction = start[:]
		direction[self.axis] -= 1
		
		if bottom_left[self.axis] < self.median[self.axis] \
				or (bottom_left[self.axis] == self.median[self.axis] and (bottom_left[1-self.axis] < self.median[1-self.axis])):
			print 'ADD_DI %f %f %f %f' % tuple(start+direction)
			print 'PLOT'
			result += self.left.getPointsInRange(bottom_left, top_right)
		else:
			print 'ADD_DJ %f %f %f %f' % tuple(start+direction)
			print 'PLOT'
		
		start = list(self.median)
		if hasattr(self.right, 'median'):
			start[self.right.axis] = self.right.median[self.right.axis]
		direction = start[:]
		direction[self.axis] += 1
		
		if self.median[self.axis] < top_right[self.axis] \
				or (self.median[self.axis] == top_right[self.axis] and self.median[1-self.axis] < top_right[1-self.axis]):
			print 'ADD_DI %f %f %f %f' % tuple(start+direction)
			print 'PLOT'
			result += self.right.getPointsInRange(bottom_left, top_right)
		else:
			print 'ADD_DJ %f %f %f %f' % tuple(start+direction)
			print 'PLOT'
		
		return result

class Empty:
	def getPointsInRange(self, a, b):
		return []

def createKDTree(points, axis=0, min1=-10., max1=10., min2=-10., max2=10.):
	if len(points) == 0:
		return Empty()
	
	axis_next = (axis + 1) % DIMENSIONS
	
	points.sort(key = itemgetter(axis))
	median_location = (len(points) - 1) / 2
	median = points[median_location]
	
	print 'ADD_DP %f %f' % median
	
	start = [0, 0]
	start[axis] = median[axis]
	start[axis_next] = min1
	
	end = [0, 0]
	end[axis] = median[axis]
	end[axis_next] = max1
	
	print 'ADD_DL %f %f %f %f' % tuple(start + end)
	print 'PLOT'
	print 'CLR_DP'
	
	return Tree(
		left = createKDTree(points[:median_location], axis_next, min2, median[axis], min1, max1),
		right = createKDTree(points[median_location+1:], axis_next, median[axis], max2, min1, max1),
		median = median,
		axis = axis
	)

def main():
	# VARIABLES
	point_list = [ (2,3), (5,4), (9,6), (4,7), (8,1), (7,2) ]
	area = ((1, 0), (5, 5))
	
	#################
	# PROGRAM BELOW #
	#################
	
	print 'PARAM set xrange [-10:10]'
	print 'PARAM set yrange [-10:10]'
	print 'INITAR type=Area'
	print 'INITPT type=Point'
	print 'INITDL type=Line'
	print 'INITDP type=Point'
	print 'INITDI type=Arrow'
	print 'INITDJ type=Arrow, title=Not checked'
	
	point_list.sort()
	
	for p in point_list:
		print 'ADD_PT %f %f' % p
	print 'PLOT'
	
	tree = createKDTree(point_list)
	print 'PLOT'
	
	print 'ADD_AR %f %f %f' % (area[0][0], area[0][1], area[1][1])
	print 'ADD_AR %f %f %f' % (area[1][0], area[0][1], area[1][1])
	print 'PLOT'
	
	print tree.getPointsInRange(*area)

if __name__ == '__main__':
	main()
