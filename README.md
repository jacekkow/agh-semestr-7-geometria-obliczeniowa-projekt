K-d trees & Quadtrees - projekt zaliczeniowy z Geometrii Obliczeniowej
======================================================================

Zadaniem było zaimplementowanie algorytmów tworzenia k-d drzew i drzew czwórkowych (quadtree)
dla punktów na płaszczyźnie oraz zaimplementowania wyszukiwania w tychże elementów znajdujących
się w zadanym prostokątnym obszarze.

Implementacja
-------------

Zadanie zostało zaimplementowane w języku Python i do działania nie wymaga dodatkowych bibliotek.
Skrypt do wizualizacji wymaga interpretera języka PHP - szczegóły dotyczące tego elementu
znajdują się w pliku ANIMATE.md.
Niniejsza dokumentacja została przygotowana dla uruchamiania programu w systemie Linux.

Uruchomienie
------------

Aby włączyć programy, należy wykonać polecenia:

    ./kdtree.py
    ./quadtree.py

lub

    python kdtree.py
    python quadtree.py

znajdując się w głównym katalogu projektu.

Uruchomienie wygeneruje dane wejściowe dla biblioteki do animacji, a w ostatniej linii wypisze
listę punktów w zadanym obszarze.

Aby od razu wygenerować wizualizację należy uruchomić aplikacje następująco:

    python kdtree.py | php animate.php

lub

    python quadtree.py | php animate.php

Spowoduje to wygenerowanie obrazków prezentujących zadanie w katalogu ./anim,
przy czym ilustracje z poprzedniego wykonania zostaną usunięte.

Modyfikacja parametrów
----------------------

### Lista punktów i obszar poszukiwań

Modyfikacji punktów dodawanych do drzew lub obszaru poszukiwań należy dokonać w plikach kdtree.py oraz quadtree.py, w liniach:

```
#!python
	# VARIABLES
	point_list = [ (2,3), (5,4), (9,6), (4,7), (8,1), (7,2) ]
	area = ((1, 0), (5, 5))
	
	#################
	# PROGRAM BELOW #
	#################
```

`point_list` to lista punktów, zaś `area` - dwa rogi obszaru, na którym mają być poszukiwane punkty
(patrz niżej).

Punkty powinny należeć do zakresu `(-10,10]x(-10x10]` - lewostronnie otwartego. Zakres poszukiwań
jest zdefiniowany przez dwa punkty - lewy dolny (w przykładzie x=1, y=0)  oraz prawy górny (x=5, y=5)
róg prostokąta.

### Quadtree - maksymalna ilość punktów w węźle

W pliku quadtree.py, na samej górze, znajduje się linia:

    MAX_POINTS = 1

Liczba 1 to maksymalna ilość punktów w węźle - można ją modyfikować według potrzeb.

K-d drzewa
----------

K-d drzewa to struktury, będące uogólnieniem drzew przeszukiwań dla wyższych wymiarów.
Kolejne poziomy drzewa odpowiadają kolejnym współrzędnym (X-owym, Y-owym itd.),
względem których będzie następował podział.

### Różnice względem wykładu

Względem k-d drzew przedstawionych na wykładzie, zaimplementowana struktura ma jedną różnicę:
punkty są przechowywane jedynie w węzłach drzewa - liście są zawsze puste. W każdym węźle
jest przechowywany jeden punkt - mediana podziału oraz dodatkowe informacje:

* oznaczenie osi podziału (0 - podział na osi X, 1 - podział na osi Y),
* wskaźnik do poddrzewa z punktami na lewo/w dół od mediany,
* wskaźnik do poddrzewa z punktami na prawo/w górę od mediany.

Jeśli którekolwiek z poddrzew jest puste, wskaźnik wskazuje specjalny element "Empty".
Taka struktura została przedstawiona w artykule na Wikipedii:
[K-d tree](http://en.wikipedia.org/wiki/K-d_tree).


Dla podziału:

![](docs/readme/kdtree-div.png)

odpowiadające drzewo wygląda następującego:

![](docs/readme/kdtree-tree.png)


### Algorytm budowania drzewa

Algorytm budowania drzewa jest rekurencyjny i działa następująco:

* sortujemy zbiór punktów względem zadanej osi,
* znajdujemy element środkowy zbioru (medianę),
* rekurencyjnie tworzymy poddrzewa lewe i prawe, wykorzystując punkty
  znajdujące się, odpowiednio, na lewo i na prawo od mediany,
* zwracamy węzeł drzewa, zawierający:
* * informację o osi podziału,
* * medianę,
* * wskaźniki do lewego i prawego poddrzewa

Wykorzystując pseudokod:

```
utwórz_drzewo(PUNKTY, OŚ=x):
    jeśli lista PUNKTY jest pusta:
        zwróć pusty liść
    
    jeśli OŚ == x:
        KOLEJNA_OŚ = y
    w p.p.:
        KOLEJNA_OŚ = x
    
    posortuj PUNKTY najpierw według osi OŚ, a w drugiej kolejności względem osi KOLEJNA_OŚ
    MEDIANA = element środkowy z punktów
    
    zwróć węzeł drzewa z następującymi elementami:
        oś: OŚ
        mediana: MEDIANA
        lewe_poddrzewo: utwórz_drzewo(PUNKTY przed MEDIANA, KOLEJNA_OŚ)
        prawe_poddrzewo: utwórz_drzewo(PUNKTY za MEDIANA, KOLEJNA_OŚ)
```

### Algorytm wyszukiwania w drzewie

Algorytm wyszukiwania punktów należących do obszaru wykonuje następujące czynności
na korzeniu drzewa:

* inicjuje pustą listę znalezionych punktów
* sprawdza, czy mediana zawiera się w obszarze,
* * jeśli tak, dołącza ją do listy
* sprawdza, czy obszar przecina się z przestrzenią lewego poddrzewa
* * jeśli tak, wykonuje się rekurencyjnie na lewym poddrzewie
    i dołącza wynik wykonania do listy
* sprawdza, czy obszar przecina się z przestrzenią prawego poddrzewa
* * jeśli tak, wykonuje się rekurencyjnie na prawym poddrzewie
    i dołącza wynik wykonania do listy
* zwraca listę

W pseudokodzie można zapisać to następująco:

```
znajdź_punkty(OBSZAR, KDDRZEWO):    
    jeśli KDDRZEWO jest pustym liściem:
        zwróć pustą listę

    WYNIK = pusta lista
    
    jeśli KDDRZEWO.mediana zawiera się w obszarze OBSZAR:
        dodaj KDDRZEWO.mediana do listy WYNIK
    jeśli OBSZAR przecina KDDRZEWO.lewe_poddrzewo:
        dodaj znajdź_punkty(OBSZAR, KDDRZEWO.lewe_poddrzewo) do listy WYNIK    
    jeśli OBSZAR przecina KDDRZEWO.prawe_poddrzewo:
        dodaj znajdź_punkty(OBSZAR, KDDRZEWO.prawe_poddrzewo) do listy WYNIK
    
    zwróć WYNIK
```

Drzewo czwórkowe (quadtree)
---------------------------

Drzewo czwórkowe pozwala przechowywać dane o punktach w przestrzeni dwuwymiarowej.
Każdy węzeł drzewa może trzymać informacje o pewnej ilości punktów. Jeśli ich ilość
przekracza zadaną wartość, węzeł jest dzielony na cztery podwęzły, odpowiadające
czterem równym ćwiartkom węzła oryginalnego, a następnie punkty są przestawiane
do odpowiednich podwęzłów.

Przykładowe drzewo dla maksymalnej ilości punktów w węźle równej 1 przedstawia obrazek:

![](docs/readme/quadtree.png)

### Tworzenie drzewa

Na początku istnieje jeden węzeł, obejmujący całą przestrzeń, w której znajdują
się punkty (zakres `(-10x10]x(-10x10]`), do którego kolejno dodawane są zadane
elementy.

Algorytm dodawania punktu do węzła działa następująco:

* jeśli węzeł jest podzielony na podwęzły, znajdź właściwy dla punktu podwęzeł i wykonaj się na nim rekurencyjnie,
* jeśli liczba punktów w węźle jest mniejsza od progu, dodaj punkt do węzła,
* jeśli liczba punktów w węźle jest większa lub równa progowi, podziel węzeł na ćwiartki i dodaj punkt do odpowiedniej.

```
dodaj_punkt(WĘZEŁ, PUNKT):
    jeśli WĘZEŁ jest podzielony na ćwiartki:
        ĆWIARTKA = znajdź ćwiartkę odpowiadającą punktowi PUNKT
        dodaj_punkt(ĆWIARTKA, PUNKT)
        zakończ
    
    jeśli liczba punktów w węźle WĘZEŁ < PRÓG:
        dodaj PUNKT do węzła WĘZEŁ
    w p.p.:
        podziel WĘZEŁ na 4 podwęzły
        zacznij niniejszą procedurę od początku
```

Podział węzła na 4 podwęzły realizuje algorytm:

```
podziel_węzeł(WĘZEŁ):
    utwórz cztery podwęzły: NW, NE, SE, SW
    dla każdego punktu w węźle WĘZEŁ:
        dodaj punkt do odpowiedniej ćwiartki
```

### Wyszukiwanie w drzewie

```
znajdź_punkty(OBSZAR, WĘZEŁ):
    jeśli WĘZEŁ jest podzielony na ćwiartki:
        wykonaj się rekurencyjnie na podwęzłach obejmujących OBSZAR
        zwróć sumę zbiorów z otrzymanych wywołań rekurencyjnych
    w p.p.:
        zwróć punkty węzła WĘZEŁ, które należą do obszaru OBSZAR
```

Bibliografia
------------

Wszystkie obrazki wykorzystane w niniejszym dokumencie pochodzą z zasobów Wikipedia Commons:

* http://commons.wikimedia.org/wiki/File:Point_quadtree.svg (Public Domain),
* http://commons.wikimedia.org/wiki/File:Kdtree_2d.svg (CC BY-SA 3.0, autor: KiwiSunset),
* http://commons.wikimedia.org/wiki/File:Tree_0001.svg (Public Domain).

W pracy korzystałem z:

* wykładów z przedmiotu,
* http://en.wikipedia.org/wiki/K-d_tree
* http://en.wikipedia.org/wiki/Quadtree
