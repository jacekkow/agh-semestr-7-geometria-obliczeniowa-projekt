Aplikacja do wizualizacji
=========================

W ramach zajęć Geometrii Obliczeniowej przygotowałem prostą aplikację,
umożliwiającą wizualizację wyników w postaci serii plików PNG,
generowanych z użyciem programu gnuplot.

Format danych wejściowych
-------------------------

Program przyjmuje na wejściu kolejne komendy tekstowe, każda w osobnej linii,
opisane poniżej. Niezrozumiałe polecenia są pomijane.

### INIT

Aby zdefiniować nową listę punktów, należy wykonać polecenie:

    INITAA type=Point

gdzie:

* `INIT` - komenda,
* `AA` - dwuznakowy identyfikator listy (dowolny, nadawany przez użytkownika,
         wielkość znaków ma znaczenie),
* `type=Point` - określenie typu listy - wymagany argument komendy.

Identyfikator listy winien być unikalny -
jest wykorzystywany w kolejnych poleceniach.

Dostępne typy list:

* `Point` - lista punktów,
* `Line` - lista odcinków,
* `Arrow` - lista wektorów (skierowanych odcinków),
* `VerticalLine` - lista pionowych prostych,
* `HorizontalLine` - lista poziomych prostych,
* `Area` - obszary.

Podanie typu spoza powyższego wykazu powoduje rzucenie wyjątku przez program.

Prócz `type` dostępne są również argumenty:

* `title` - opis listy (`title` w gnuplot)
* `color` - kolor wyświetlania elementów z listy (`linecolor` w gnuplot)

Kolejne opcje należy rozdzielać przecinkami, na przykład
(komenda inicjuję listę punktów kolorowanych na zielono):

    INITAA type=Point, color=green

#### Uwaga dotycząca prostch    

Ponieważ gnuplot nie umożliwia rysowania "prawdziwych" prostych,
są one wyświetlane jako odcinki. Początkową współrzędną oraz długość "wirtualnej prostej"
można ustawić dla typów `VerticalLine` i `HorizontalLine` poprzez dodatkowe argumenty:
* start - początkowa współrzędna (X-owa dla HorizontalLine, Y-owa dla VerticalLine; domyślnie -10),
* length - długość wizualizowanej prostej (domyślnie 20).

Aby wyświetlać poziome linie długości 10 i początku w X=0, należy wpisać:

    INITXA type=HorizontalLine, start=0, length=10

### ADD_

Do zadanej listy możemy dodawać właściwe elementy komendą:

    ADD_AA 1.2 2.5

Co spowoduje dodanie punktu o współrzędnych X=1.2, Y=2.5 na końcu listy o identyfikatorze AA.
Lista ta winna być wcześniej zainicjowana komendą `INIT`.

Poszczególne typy list wymagają następujących argumentów dla komendy `ADD_`:

| Typ            | Argumenty   | Opis |
|:---------------|:------------|:-----|
| Point          | X Y         | Współrzędne (X, Y) punktu |
| Line           | X1 Y1 X2 Y2 | Współrzędne końców linii  |
| Arrow          | X1 Y1 X2 Y2 | Współrzędne punktu zaczepienia (X1, Y1) i końca (X2, Y2) wektora |
| VerticalLine   | X           | Współrzędna X prostej |
| HorizontalLine | Y           | Współrzędna Y prostej |
| Area           | X Y1 Y2     | Obszar, który na współrzędnej X zawiera się między Y1 a Y2 |

### DEL_

Aby usunąć ostatni element listy, można skorzystać z funkcji `DEL_`:

    DEL_AA

Komenda ta nie przyjmuje argumentów.

### CLR_

Dostępna jest bezargumentowa funkcja umożliwiającą wyczyszczenie listy
(tj. usunięcia wszystkich jej elementów):

    CLR_AA

### PLOT

Komenda przekazanie danych do komendy gnuplot, w celu wygenerowania
jednej "klatki" wizualizacji z danymi przekazanymi do tej pory.

    PLOT

Komendę można używać wielokrotnie - za każdym razem zostanie utworzony
kolejny obrazek z właściwymi elementami.

### PARAM

Program umożliwia przekazanie danych bezpośrednio do gnuplota.
Służy do tego komenda:
    PARAM set xrange [-10:10]
Przekazanie danych z powyższego przykładu spowoduje, że w skrypcie
przekazywanym do gnuplota znajdzie się linia:
    set xrange [-10:10]
Ustawienie będzie miało wpływ wyłącznie na kolejne wywołania `PLOT`
i nie zmienia już wygenerowanych obrazków, stąd aby dotyczyła
wszystkich klatek wizualizacji, zalecane jest jej wykonanie
przed komendą `INIT`.

Przykłady
---------

### Przykład 1: Odcinek od (1,1) do (2,3)

```
INITLI type=Line
ADD_LI 1 1 2 3
PLOT
```

Efekt:

![](docs/anim_ex/1.png)

### Przykład 2: Wektor od (2,2) do (3,4)

```
INITVE type=Arrow
ADD_VE 2 2 3 4
PLOT
```

Efekt:

![](docs/anim_ex/2.png)

### Przykład 3: pozioma linia na wysokości Y=3
```
INITHL type=HorizontalLine
ADD_HL 3
PLOT
```

Efekt:

![](docs/anim_ex/3.png)

### Przykład 4: pionowa linia na szerokości X=2
```
INITVL type=VerticalLine
ADD_VL 2
PLOT
```

Efekt:

![](docs/anim_ex/4.png)

### Przykład 5: prostokąt od (1,0) do (3,2) - boki równoległe do osi
```
INITAR type=Area
ADD_AR 1 0 2
ADD_AR 3 0 2
PLOT
```

**Wyjaśnienie:** w punkcie X=1 obszar zawiera się między Y1=0 i Y2=2 i rozciąga się do X=3,
gdzie również zawiera się między Y1=0 i Y2=2

Efekt:

![](docs/anim_ex/5.png)


### Przykład 6: Złożenie kilku poleceń

```
PARAM set xrange [-10:10]
PARAM set yrange [-10:10]

INITAA type=Point
INITBB type=Line
INITCC type=Arrow
INITDD type=VerticalLines, start=-5, length=11
INITEE type=HorizontalLine
INITFF type=Area

ADD_AA 1 2
ADD_FF -5 3 5
ADD_FF -3 3 6
PLOT

ADD_AA 1 3
ADD_BB -8 -6 -7 -4
ADD_CC -7 -5 -6 -3
PLOT

DEL_AA
ADD_AA 1 4
ADD_DD -1
ADD_EE 0
PLOT

CLR_AA
CLR_EE
PLOT
```

Powyższy skrypt spowoduje wygenerowanie następujących obrazków:

![](docs/anim_ex6/0001.png)
![](docs/anim_ex6/0002.png)
![](docs/anim_ex6/0003.png)
![](docs/anim_ex6/0004.png)


Przekształcanie pliku wejściowego w wizualizację
------------------------------------------------

Skrypt przekształcający został napisany w języku PHP, stąd do jego wykonania
konieczny jest interpreter PHP.

Plik wejściowy należy podać na standardowe wejście skryptu.
Obrazki zostaną wygenerowane do folderu ./anim w katalogu ze skryptem.

    php animate.php < plik_wejsciowy

**Uwaga!** Jeśli katalog ./anim już istnieje, to zostaną z niego usunięte
wszystkie pliki *.png!

Możliwe jest przekierowanie wyjścia programu generującego dane we właściwym formacie
bezpośrednio do skryptu:

    ./kdtree.py | php animate.php
