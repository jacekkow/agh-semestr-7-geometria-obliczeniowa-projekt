#!/usr/bin/env python

MAX_POINTS = 1

class QuadTree:
	def __init__(self, bottom_left, top_right):
		self.bottom_left = (float(bottom_left[0]), float(bottom_left[1]))
		self.top_right = (float(top_right[0]), float(top_right[1]))
		self.center = (
			(bottom_left[0]+top_right[0])/2,
			(bottom_left[1]+top_right[1])/2
		)
		
		self.points = []
		self.NE = None
		self.NW = None
		self.SE = None
		self.SW = None
	
	def addPoint(self, point, ignore = False):
		if not ignore:
			print 'ADD_DP %f %f' % point
			print 'PLOT'
		
		point = (float(point[0]), float(point[1]))
		if self.NE != None:
			if point[0] <= self.center[0]:
				if(point[1] <= self.center[1]):
					self.SW.addPoint(point, True)
				else:
					self.NW.addPoint(point, True)
			else:
				if(point[1] <= self.center[1]):
					self.SE.addPoint(point, True)
				else:
					self.NE.addPoint(point, True)
		elif len(self.points) <= MAX_POINTS - 1:
			self.points.append(point)
		else:
			self.divide()
			self.addPoint(point, True)
	
	def divide(self):
		center_top = (self.center[0], self.top_right[1])
		center_right = (self.top_right[0], self.center[1])
		center_left = (self.bottom_left[0], self.center[1])
		center_bottom = (self.center[0], self.bottom_left[1])
		
		self.NW = QuadTree(center_left, center_top)
		self.NE = QuadTree(self.center, self.top_right)
		self.SW = QuadTree(self.bottom_left, self.center)
		self.SE = QuadTree(center_bottom, center_right)
		
		for p in self.points:
			self.addPoint(p, True)
		
		self.points = None
		
		print 'ADD_DL %f %f %f %f' % (self.center[0], self.bottom_left[1], self.center[0], self.top_right[1])
		print 'ADD_DL %f %f %f %f' % (self.bottom_left[0], self.center[1], self.top_right[0], self.center[1])
		print 'PLOT'
	
	def getPointsInRange(self, bottom_left, top_right, direction='X'):
		bottom_left = (float(bottom_left[0]), float(bottom_left[1]))
		top_right = (float(top_right[0]), float(top_right[1]))
		
		result = []
		
		print_dir = {
				'X':  lambda x : '',
				'NE': lambda x : 'ADD_D%s %f %f %f %f' % (x, self.bottom_left[0], self.bottom_left[1], self.bottom_left[0]+1, self.bottom_left[1]+1),
				'NW': lambda x : 'ADD_D%s %f %f %f %f' % (x, self.top_right[0], self.bottom_left[1], self.top_right[0]-1, self.bottom_left[1]+1),
				'SE': lambda x : 'ADD_D%s %f %f %f %f' % (x, self.bottom_left[0], self.top_right[1], self.bottom_left[0]+1, self.top_right[1]-1),
				'SW': lambda x : 'ADD_D%s %f %f %f %f' % (x, self.top_right[0], self.top_right[1], self.top_right[0]-1, self.top_right[1]-1),
			};
		
		if top_right[0] <= self.bottom_left[0] \
				or top_right[1] <= self.bottom_left[1] \
				or self.top_right[0] < bottom_left[0] \
				or self.top_right[1] < bottom_left[1]:
			print print_dir[direction]('J')
			return result
		
		print print_dir[direction]('I')
		print 'PLOT'
		
		if self.NE != None:
			result += self.NE.getPointsInRange(bottom_left, top_right, 'NE')
			result += self.NW.getPointsInRange(bottom_left, top_right, 'NW')
			result += self.SE.getPointsInRange(bottom_left, top_right, 'SE')
			result += self.SW.getPointsInRange(bottom_left, top_right, 'SW')
		else:
			for p in self.points:
				if bottom_left[0] <= p[0] and p[0] <= top_right[0] \
						and bottom_left[1] <= p[1] and p[1] <= top_right[1]:
					print 'ADD_DP %f %f' % p
					result.append(p)
				print 'PLOT'
		
		return result

def main():
	# VARIABLES
	point_list = [ (2,3), (5,4), (9,6), (4,7), (8,1), (7,2) ]
	area = ((1, 0), (5, 5))
	
	#################
	# PROGRAM BELOW #
	#################
	
	print 'PARAM set xrange [-10:10]'
	print 'PARAM set yrange [-10:10]'
	print 'INITAR type=Area'
	print 'INITPT type=Point'
	print 'INITDL type=Line'
	print 'INITDP type=Point'
	print 'INITDI type=Arrow'
	print 'INITDJ type=Arrow, title=Not checked'
	
	for p in point_list:
		print 'ADD_PT %f %f' % p
	
	tree = QuadTree((-10, -10), (10, 10))
	for p in point_list:
		tree.addPoint(p)
	
	print 'CLR_DP'
	print 'PLOT'
	
	print 'ADD_AR %f %f %f' % (area[0][0], area[0][1], area[1][1])
	print 'ADD_AR %f %f %f' % (area[1][0], area[0][1], area[1][1])
	
	print tree.getPointsInRange(*area)

if __name__ == '__main__':
	main()
