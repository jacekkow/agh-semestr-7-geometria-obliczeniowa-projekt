<?php
class PlotGenerator {
	public $iteration = 1;
	
	public $params = '';
	public $elements = array();
	public $types = array();
	
	public $dir;
	
	public function __construct($dir = NULL) {
		if($dir === NULL) $dir = dirname(__FILE__).'/anim';
		$this->dir = $dir;
	}
	
	public function clearDirectory() {
		@mkdir($this->dir);
		foreach(glob($this->dir.'/*.png') as $file) {
			unlink($file);
		}
	}
	
	public function generatePlot() {
		$iteration = str_pad($this->iteration++, 4, '0', STR_PAD_LEFT);
		$fileprefix = $this->dir.'/'.$iteration;
		
		$dataFile = '';
		$gnuplotScript = $this->params.'
set term png
set output \''.$fileprefix.'.png\'

plot';
		
		$index = 0;
		foreach($this->types as $type => $data) {
			if(count($this->elements[$type]) > 0) {
				$dataFile .= join("\n", $this->elements[$type])."\n\n\n";
				$gnuplotScript .= ($index == 0 ? '' : ',').' \\'."\n".'\''.$fileprefix.'.txt\' index '.$index.' '.$data;
				$index++;
			}
		}
		$gnuplotScript .= "\n";
		file_put_contents($fileprefix.'.txt', $dataFile);
		unset($dataFile);
		
		$gnuplot = popen('gnuplot', 'w');
		fwrite($gnuplot, $gnuplotScript);
		pclose($gnuplot);
		
		unlink($fileprefix.'.txt');
	}
	
	public function command($cmd, $element, $args = NULL) {
		switch($cmd) {
			case 'CLR_':
				$this->elements[$element] = array();
				break;
			case 'ADD_':
				array_push($this->elements[$element], $args);
				break;
			case 'DEL_':
				array_pop($this->elements[$element]);
				break;
		}
	}
	
	public function readline($input) {
		while(!feof($input)) {
			$line = fgets($input, 256);
			if(!$line) return FALSE;
			$line = trim($line);
			if(strlen($line) == 0) continue;
			return $line;
		}
	}
	
	public function parseArgs($args) {
		$result = array();
		
		$args = explode(',', $args);
		foreach($args as $arg) {
			$arg = trim($arg);
			if(strpos($arg, '=') != FALSE) {
				$arg = explode('=', $arg, 2);
				$result[$arg[0]] = $arg[1];
			} else {
				$result[$arg] = TRUE;
			}
		}
		
		return $result;
	}
	
	public function getArg($args, $name, $default = '') {
		if(isset($args[$name])) {
			return $args[$name];
		}
		return $default;
	}
	
	public function process($input = STDIN) {
		while($line = $this->readline($input)) {
			$cmd = strtoupper(substr($line, 0, 4));
			$array = strtoupper(substr($line, 4, 2));
			$rest = trim(substr($line, 6));
			
			if($cmd == 'PARA' && $array == 'M ') {
				$this->params .= $rest."\n";
			} elseif($cmd == 'INIT') {
				$args = $this->parseArgs($rest);
				
				$gnuplotArgs = '';
				if(isset($args['color'])) {
					$gnuplotArgs .= ' lc \''.$args['color'].'\'';
				}
				
				switch(strtolower($args['type'])) {
					case 'area':
					case 'areas':
						$this->types[$array] = 'using 1:2:3 title \''.$this->getArg($args, 'title', 'areas').'\' with filledcurves'
							.' fs transparent solid 0.2';
						break;
					case 'vertical':
					case 'verticalline':
					case 'verticallines':
						$this->types[$array] = 'using 1:('.$this->getArg($args, 'start', '-10').'):(0):('.$this->getArg($args, 'length', '20').')'
							.' title \''.$this->getArg($args, 'title', 'vertical lines').'\' with vectors nohead'.$gnuplotArgs;
						break;
					case 'horizontal':
					case 'horizontalline':
					case 'horizontallines':
						$this->types[$array] = 'using ('.$this->getArg($args, 'start', '-10').'):1:('.$this->getArg($args, 'length', '20').'):(0)'
							.' title \''.$this->getArg($args, 'title', 'horizontal lines').'\' with vectors nohead'.$gnuplotArgs;
						break;
					case 'line':
					case 'lines':
						$this->types[$array] = 'using 1:2:($3-$1):($4-$2)'
							.' title \''.$this->getArg($args, 'title', 'lines').'\' with vectors nohead'.$gnuplotArgs;
						break;
					case 'point':
					case 'points':
						$this->types[$array] = 'using 1:2 title \''.$this->getArg($args, 'title', 'points').'\' with points'.$gnuplotArgs;
						break;
					case 'arrow':
					case 'arrows':
						$this->types[$array] = 'using 1:2:($3-$1):($4-$2)'
							.' title \''.$this->getArg($args, 'title', 'arrows').'\' with vectors'.$gnuplotArgs;
						break;
					default:
						throw new Exception('Unknown type: '.$args['type'].' for animation');
				}
				$this->elements[$array] = array();
			} elseif($cmd == 'PLOT') {
				$this->generatePlot();
			} else {
				$this->command($cmd, $array, $rest);
			}
		}
	}
}

$proc = new PlotGenerator();
$proc->clearDirectory();
$proc->process(STDIN);
