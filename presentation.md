K-d tree & quadtree
===================

Projekt zaliczeniowy z przedmiotu Geometria obliczeniowa

!

Problem
-------

Dane wejściowe:

* P - zbiór punktów na płaszczyźnie
* O - prostokątny obszar

Znajdź punkty należące do obszaru.

!

Rozwiązanie
-----------

Jeśli zbiór punktów się nie zmienia, a ilość zapytań jest duża
- liniowe przeszukiwanie jest za wolne!

Użyjemy specjalnych struktur.

!

K-d drzewo
----------

Drzewo przedziałowe, którego każdy poziom odpowiada kolejnej współrzędnej.

![](docs/readme/kdtree-tree.png)

!

Tworzenie k-d drzewa
--------------------

* znajdujemy element środkowy zbioru (medianę),
* rekurencyjnie tworzymy poddrzewa lewe i prawe z punktów na lewo i na prawo od mediany,
* zwracamy węzeł drzewa, zawierający:
  * informację o osi podziału,
  * medianę,
  * wskaźniki do poddrzew

!

```
utwórz_drzewo(PUNKTY, OŚ=x):
  jeśli lista PUNKTY jest pusta:
    zwróć pusty liść
  
  MEDIANA = element środkowy z PUNKTY
  
  zwróć węzeł drzewa ze składowymi:
    oś: OŚ
    mediana: MEDIANA
    lewe_poddrzewo:
      utwórz_drzewo(PUNKTY przed MEDIANA, OŚ + 1)
    prawe_poddrzewo:
      utwórz_drzewo(PUNKTY za MEDIANA, OŚ + 1)
```

!

Wyszukiwanie w k-d drzewie
--------------------------

```
znajdź_punkty(OBSZAR, WĘZEŁ):
  WYNIK = pusta lista
    
  jeśli MEDIANA węzła należy do OBSZAR:
    dodaj MEDIANA do listy WYNIK
  jeśli OBSZAR przecina lewe poddrzewo:
    dodaj znajdź_punkty(OBSZAR, lewe_poddrzewo)
      do listy WYNIK    
  jeśli OBSZAR przecina prawe poddrzewo:
    dodaj znajdź_punkty(OBSZAR, prawe_poddrzewo)
      do listy WYNIK

```

!

Quadtree
--------

Drzewo, w którym każdy węzeł odpowiada pewnemu prostokątnemu obszarowi płaszczyzny.

![](docs/readme/quadtree-min.png)

!

Tworzenie drzewa czwórkowego
----------------------------

Kolejno dodajemy punkty do korzenia, zgodnie z algorytmem:

* jeśli węzeł ma podwęzły:
  * wstaw punkt do odpowiedniego,
* jeśli węzeł może przyjąć punkt:
  * dodaj punkt do węzła,
* jeśli węzeł ma za dużo punktów:
  * podziel węzeł na ćwiartki i dodaj punkt do odpowiedniej.

!

```
dodaj_punkt(WĘZEŁ, PUNKT):
  jeśli WĘZEŁ jest podzielony:
    ĆWIARTKA = znajdź ćwiartkę dla punktu
    dodaj_punkt(ĆWIARTKA, PUNKT)
    zakończ
    
  jeśli liczba punktów w węźle WĘZEŁ < PRÓG:
    dodaj PUNKT do węzła WĘZEŁ
  w p.p.:
    podziel WĘZEŁ na 4 podwęzły
    zacznij niniejszą procedurę od początku
```

!

```
podziel_węzeł(WĘZEŁ):
    utwórz cztery podwęzły: NW, NE, SE, SW
    dla każdego punktu w węźle WĘZEŁ:
        dodaj punkt do odpowiedniej ćwiartki
```

!

Wyszukiwanie w quadtree
-----------------------

```
znajdź_punkty(OBSZAR, WĘZEŁ):
    jeśli WĘZEŁ jest podzielony na ćwiartki:
        wykonaj się rekurencyjnie
          na podwęzłach obejmujących OBSZAR
        zwróć sumę zbiorów
          z wywołań rekurencyjnych
    w p.p.:
        zwróć punkty węzła WĘZEŁ,
          które należą do obszaru OBSZAR
```

!

Bibliografia
------------

W pracy korzystałem z:

* wykładów z przedmiotu,
* http://en.wikipedia.org/wiki/K-d_tree
* http://en.wikipedia.org/wiki/Quadtree
